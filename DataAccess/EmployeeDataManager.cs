﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataAccess
{
    public class EmployeeDataManager
    {
        public async Task<List<dynamic>> GetEmployeeDtoListAsync(int id = 0)
        {
            List<dynamic> employeeList = null;
            using (HttpClient httpClient = new HttpClient())
            {
                HttpResponseMessage response = await httpClient.GetAsync("http://masglobaltestapi.azurewebsites.net/api/Employees");
                if (response.IsSuccessStatusCode)
                {
                    employeeList = await response.Content.ReadAsAsync<List<dynamic>>();
                }
            }

            if (id > 0)
            {
                employeeList = employeeList.Where(e => e.id == id).ToList();
            }

            return employeeList;
        }
    }
}
