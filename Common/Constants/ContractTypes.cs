﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Constants
{
    public class ContractTypes
    {
        public const string hourlySalaryEmployee = "HourlySalaryEmployee";
        public const string monthlySalaryEmployee = "MonthlySalaryEmployee";
    }
}
