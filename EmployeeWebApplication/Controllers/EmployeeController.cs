﻿using Business;
using Factory.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace EmployeeWebApplication.Controllers
{
    public class EmployeeController : ApiController
    {
        // GET api/<controller>
        public async Task<List<IEmployee>> Get(int id = 0)
        {
            EmployeeBusiness business = new EmployeeBusiness();
            return await business.GetEmployeesAsync(id);
        }
    }
}