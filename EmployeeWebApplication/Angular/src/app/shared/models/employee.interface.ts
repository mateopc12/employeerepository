export interface IEmployee {
  Id: number;
  Name: string;
  ContractTypeName: string;
  RoleName: string;
  RoleDescription: string;
  HourlySalary: number;
  MonthlySalary: number;
  AnnualSalary: number;
}
