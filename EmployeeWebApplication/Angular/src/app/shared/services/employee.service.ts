import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { apiURL } from '../constants';
import { Observable } from 'rxjs';

@Injectable()
export class EmployeeService {
  constructor(private http: HttpClient) {}

  getEmployee(id: string): Observable<any> {
    if (id) {
      return this.http.get(`${apiURL}/api/employee?id=${id}`);
    }

    return this.http.get(`${apiURL}/api/employee`);
  }
}
