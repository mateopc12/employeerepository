import { EmployeeService } from './shared/services/employee.service';
import { Component } from '@angular/core';
import { IEmployee } from './shared/models/employee.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  employeeId = '';
  isBusy: any;
  showError: boolean;
  employeeList: IEmployee[] = [];

  constructor(private employeeService: EmployeeService) {
    this.isBusy = undefined;
    this.getEmployeeList();
  }

  getEmployeeList() {
    this.showError = false;
    this.isBusy = this.employeeService.getEmployee(this.employeeId).subscribe(
      (employees: IEmployee[]) => {
        this.employeeList = employees;
        this.isBusy = undefined;
      },
      (error) => {
        this.isBusy = undefined;
        this.showError = true;
      },
    );
  }
}
