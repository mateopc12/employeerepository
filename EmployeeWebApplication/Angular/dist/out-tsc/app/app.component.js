import * as tslib_1 from "tslib";
import { EmployeeService } from './shared/services/employee.service';
import { Component } from '@angular/core';
var AppComponent = /** @class */ (function () {
    function AppComponent(employeeService) {
        this.employeeService = employeeService;
        this.employeeId = '';
        this.employeeList = [];
        this.isBusy = undefined;
        this.getEmployeeList();
    }
    AppComponent.prototype.getEmployeeList = function () {
        var _this = this;
        this.showError = false;
        this.isBusy = this.employeeService.getEmployee(this.employeeId).subscribe(function (employees) {
            _this.employeeList = employees;
            _this.isBusy = undefined;
        }, function (error) {
            _this.isBusy = undefined;
            _this.showError = true;
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css'],
        }),
        tslib_1.__metadata("design:paramtypes", [EmployeeService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map