﻿using Common.Constants;
using Factory.Employees.Managers;
using Factory.Interfaces;
using System;

namespace Factory
{
    public class EmployeeFactory
    {
        public static IEmployee GetEmployeeSalary(dynamic employeeObj)
        {
            IEmployee employee = null;
            switch (employeeObj.contractTypeName.Value)
            {
                case ContractTypes.hourlySalaryEmployee:
                    employee = new HourlySalaryEmployee(employeeObj);
                    break;
                case ContractTypes.monthlySalaryEmployee:
                    employee = new MonthlySalaryEmployee(employeeObj);
                    break;
                default: throw new ArgumentException(string.Format("Contract type {0} is not configured", employeeObj.contractTypeName.Value));
            }

            return employee;
        }
    }
}
