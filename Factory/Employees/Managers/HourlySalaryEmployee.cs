﻿using Common;
using Factory.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory.Employees.Managers
{
    public class HourlySalaryEmployee : IEmployee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContractTypeName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int HourlySalary { get; set; }
        public int MonthlySalary { get; set; }
        public int AnnualSalary
        {
            get { return 120 * HourlySalary * 12; }
        }

        public HourlySalaryEmployee(dynamic dto)
        {
            Id = dto.id;
            Name = dto.name;
            ContractTypeName = dto.contractTypeName;
            RoleId = dto.roleId;
            RoleName = dto.roleName;
            RoleDescription = dto.roleDescription;
            HourlySalary = dto.hourlySalary;
            MonthlySalary = dto.monthlySalary;

        }
    }
}
