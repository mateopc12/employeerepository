﻿using DataAccess;
using Factory;
using Factory.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business
{
    public class EmployeeBusiness
    {
        public async Task<List<IEmployee>> GetEmployeesAsync(int id = 0)
        {
            List<IEmployee> employeeList = new List<IEmployee>();
            EmployeeDataManager manager = new EmployeeDataManager();
            var employees = await manager.GetEmployeeDtoListAsync(id);

            employees.ForEach(employee =>
            {
                var employeeFactory = EmployeeFactory.GetEmployeeSalary(employee);
                employeeList.Add(employeeFactory);
            });

            return employeeList;
        }
    }
}
